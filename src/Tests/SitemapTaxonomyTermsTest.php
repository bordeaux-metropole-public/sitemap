<?php

namespace Drupal\sitemap\Tests;

/**
 * Tests the display of taxonomies based on sitemap settings.
 *
 * @group sitemap
 */
class SitemapTaxonomyTermsTest extends SitemapTaxonomyTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create terms.
    $this->terms = $this->createTerms($this->vocabulary);

  }

  /**
   * Tests the term_threshold setting
   */
  public function testTermThreshold() {
    // Get term names from terms.
    $names = [];
    foreach ($this->terms as $term) {
      $names[] = $term->label();
    }

    // Confirm that terms without content are not displayed by default.
    $this->drupalGet('sitemap');
    foreach ($names as $term_name) {
      $this->assertSession()->pageTextNotContains($term_name);
    }

    // Show all taxonomy terms, even if they are not assigned to any nodes.
    $edit = [
      'term_threshold' => -1,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Assert that terms without nodes are now displayed on the sitemap.
    $this->drupalGet('sitemap');
    foreach ($names as $term_name) {
      $this->assertSession()->pageTextContains($term_name);
      $this->assertSession()->linkNotExists($term_name);
    }

    // Create test node with terms.
    $this->createNodeWithTerms($this->terms);
    drupal_flush_all_caches();

    // Assert that terms with content are displayed on the sitemap as links when
    // term_threshold is set to -1.
    $this->drupalGet('sitemap');
    foreach ($names as $term_name) {
      $this->assertSession()->linkExists($term_name);
    }

    // Require at least one node for taxonomy terms to show up.
    $edit = [
      'term_threshold' => 0,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Assert that terms with content are displayed on the sitemap as links.
    $this->drupalGet('sitemap');
    foreach ($names as $term_name) {
      $this->assertSession()->linkExists($term_name);
    }

    // Require at least two nodes for taxonomy terms to show up.
    $edit = [
      'term_threshold' => 1,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    $terms = $this->terms;
    unset($terms[0]);

    // Create a second test node with only two terms.
    $this->createNodeWithTerms($terms);

    $this->drupalGet('sitemap');
    $this->assertSession()->linkNotExists($this->terms[0]->label());
    $this->assertSession()->linkExists($this->terms[1]->label());
    $this->assertSession()->linkExists($this->terms[2]->label());
  }

  /**
   * Tests appearance of node counts.
   */
  public function testNodeCounts() {

    // Create test node with terms.
    $this->createNodeWithTerms($this->terms);

    // Assert that node counts are included in the sitemap by default.
    $this->drupalGet('/sitemap');
    $this->assertEquals(substr_count($this->getTextContent(), '(1)'), 3, 'Node counts are included');

    // Configure module to hide node counts.
    $edit = [
      'show_count' => FALSE,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Assert that node counts are not included in the sitemap.
    $this->drupalGet('sitemap');
    $this->assertEquals(substr_count($this->getTextContent(), '(1)'), 0, 'Node counts are not included');
  }

  /**
   * Tests vocabulary depth settings.
   */
  public function testVocabularyDepth() {

    // Set to show all taxonomy terms, even if they are not assigned to any
    // nodes.
    $edit = [
      'term_threshold' => -1,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Get tags from terms.
    $tags = [];
    foreach ($this->terms as $term) {
      $tags[] = $term->label();
    }

    // Change vocabulary depth to -1.
    $edit = [
      'vocabulary_depth' => -1,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Assert that all tags are listed in the sitemap.
    $this->drupalGet('sitemap');
    foreach ($tags as $tag) {
      $this->assertSession()->pageTextContains($tag);
    }

    // Change vocabulary depth to 0.
    $edit = [
      'vocabulary_depth' => 0,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Assert that no tags are listed in the sitemap.
    $this->drupalGet('sitemap');
    foreach ($tags as $tag) {
      $this->assertSession()->pageTextNotContains($tag);
    }

    // Change vocabulary depth to 1.
    $edit = [
      'vocabulary_depth' => 1,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Assert that only tag 1 is listed in the sitemap.
    $this->drupalGet('sitemap');
    $this->assertSession()->pageTextContains($tags[0]);
    $this->assertSession()->pageTextNotContains($tags[1]);
    $this->assertSession()->pageTextNotContains($tags[2]);

    // Change vocabulary depth to 2.
    $edit = [
      'vocabulary_depth' => 2,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Assert that tag 1 and tag 2 are listed in the sitemap.
    $this->drupalGet('sitemap');
    $this->assertSession()->pageTextContains($tags[0]);
    $this->assertSession()->pageTextContains($tags[1]);
    $this->assertSession()->pageTextNotContains($tags[2]);

    // Change vocabulary depth to 3.
    $edit = [
      'vocabulary_depth' => 3,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Assert that all tags are listed in the sitemap.
    $this->drupalGet('sitemap');
    foreach ($tags as $tag) {
      $this->assertSession()->pageTextContains($tag);
    }
  }

  /**
   * Tests vocabulary show link settings.
   */
  public function testVocabularyShowLinks() {
    // Set to show all taxonomy terms, even if they are not assigned to any
    // nodes. Also change the vocabulary depth to -1.
    $edit = [
      'term_threshold' => -1,
      'vocabulary_depth' => -1,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Get tags from terms.
    $tags = [];
    foreach ($this->terms as $term) {
      $tags[] = $term->label();
    }

    // Assert that all tags are listed in the sitemap and do not link.
    $this->drupalGet('sitemap');
    foreach ($tags as $tag) {
      $this->assertSession()->pageTextContains($tag);
      $this->assertSession()->linkNotExists($tag);
    }

    // Configure to show all links.
    $edit = [
      'vocabulary_show_links' => TRUE,
    ];
    $this->submitForm('admin/config/search/sitemap', $edit, t('Save configuration'));

    // Now assert that all terms have links on them.
    $this->drupalGet('sitemap');
    foreach ($tags as $tag) {
      $this->assertSession()->linkExists($tag);
    }
  }

}
